package hello;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class ResourceNotFoundException extends RuntimeException {

}

@Controller
public class CvController {

    @GetMapping("/about_me")
    public String aboutMe(@RequestParam(name = "name", required = false)
                                  String name, Model model) {

        if (name == null || name.equals("")) {
            model.addAttribute("name", "This is my CV");
        } else if (!name.matches("[a-zA-z]+")) {

            throw new ResourceNotFoundException();

        } else {
            model.addAttribute("name", name + ", I hope you're interested to hire me");
        }
        return "about_me";
    }

}
