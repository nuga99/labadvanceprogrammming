package hello;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CvController.class)
public class CvControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void greetingUserCv() throws Exception {
        mockMvc.perform(get("/about_me").param("name", "Ronaldo"))
                .andExpect(content().string(containsString("Ronaldo, I hope you&#39;re"
                        + " interested to hire me")))
                .andExpect(content().string(not("This is my CV")));
    }


    @Test
    public void greetingWithoutUserCv() throws Exception {

        mockMvc.perform(get("/about_me").param("name", ""))
                .andExpect(content().string(containsString("This is my CV")))
                .andExpect(content().string(not(containsString("I hope you&#39;re"
                        + "interested to hire me"))));
    }

    @Test
    public void validCvName() throws Exception {

        mockMvc.perform(get("/about_me"))
                .andExpect(content().string(containsString("Muhammad Ardivan Satrio Nugroho")))
                .andExpect(content().string(not(containsString("Bobby Didi"))));

    }

    @Test
    public void validBirthDate() throws Exception {

        mockMvc.perform(get("/about_me"))
                .andExpect(content().string(containsString("14 Agustus 1999")))
                .andExpect(content().string(not(containsString("1 Januari 1443"))));

    }

    @Test
    public void validAddress() throws Exception {

        mockMvc.perform(get("/about_me"))
                .andExpect(content().
                        string(containsString("Jalan Rawajati Barat V, no.26, Jakarta Selatan")))
                .andExpect(content().string(not(containsString("Jalan warna warni"))));
    }

    @Test
    public void validEducation() throws Exception {

        mockMvc.perform(get("/about_me"))
                .andExpect(content().
                        string(containsString("Universitas of Indonesia")))
                .andExpect(content().string(containsString("SMAN 28 Jakarta")));
    }

}