package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class CompositeMain {




    public static void main(String[] args) {

        Company company;
        Ceo luffy;
        Cto zorro;
        BackendProgrammer franky;
        BackendProgrammer usopp;
        FrontendProgrammer nami;
        FrontendProgrammer robin;
        UiUxDesigner sanji;
        NetworkExpert brook;
        SecurityExpert chopper;

        company = new Company();

        luffy = new Ceo("Luffy", 500000.00);
        company.addEmployee(luffy);

        zorro = new Cto("Zorro", 320000.00);
        company.addEmployee(zorro);

        franky = new BackendProgrammer("Franky", 94000.00);
        company.addEmployee(franky);

        usopp = new BackendProgrammer("Usopp", 200000.00);
        company.addEmployee(usopp);

        nami = new FrontendProgrammer("Nami",66000.00);
        company.addEmployee(nami);

        sanji = new UiUxDesigner("sanji", 177000.00);
        company.addEmployee(sanji);

        brook = new NetworkExpert("Brook", 83000.00);
        company.addEmployee(brook);

        chopper = new SecurityExpert("Chopper", 70000.00);
        company.addEmployee(chopper);


        company.getAllEmployees().
                stream().
                map(emp -> emp.getName() + " " + emp.getRole() + " " + emp.getSalary()).
                forEach(System.out::println);




    }
}
