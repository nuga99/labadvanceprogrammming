package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {

    public NetworkExpert(String name, double salary) {

        this.name = name;
        this.salary = salary;
        if (this.salary < 50000.00) {
            throw new IllegalArgumentException();
        }
        this.role = "Network Expert";

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getSalary() {
        return salary;
    }

    @Override
    public String getRole() {
        return role;
    }
}
