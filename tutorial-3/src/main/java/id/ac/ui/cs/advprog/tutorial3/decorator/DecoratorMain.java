package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorMain {

    public static void main(String[] args) {

        Food thickBunBurgerSpecial;

        thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();
        System.out.println(thickBunBurgerSpecial.getDescription());
        System.out.println(thickBunBurgerSpecial.cost());
        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription());
        System.out.println(thickBunBurgerSpecial.cost());
        thickBunBurgerSpecial = FillingDecorator.CHICKEN_MEAT.addFillingToBread(thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription());
        System.out.println(thickBunBurgerSpecial.cost());


    }
}
