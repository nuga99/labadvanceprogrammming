package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class GiantClam implements Clams {

    public String toString() {
        return "Giant Clam";
    }
}
