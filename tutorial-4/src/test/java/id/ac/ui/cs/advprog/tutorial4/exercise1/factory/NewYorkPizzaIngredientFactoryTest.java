package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import java.util.stream.IntStream;
import org.junit.Before;
import org.junit.Test;



public class NewYorkPizzaIngredientFactoryTest {

    private NewYorkPizzaIngredientFactory nyPizzaFactory;

    @Before
    public void setUp() {

        nyPizzaFactory = new NewYorkPizzaIngredientFactory();
    }

    @Test
    public void testCreateDough() {

        ThinCrustDough thinCrustDough = new ThinCrustDough();
        assertEquals(thinCrustDough.getClass(), nyPizzaFactory.createDough().getClass());
        assertEquals(thinCrustDough.toString(), nyPizzaFactory.createDough().toString());

    }

    @Test
    public void testCreateCheese() {

        ReggianoCheese reggianoCheese = new ReggianoCheese();
        assertEquals(reggianoCheese.getClass(),  nyPizzaFactory.createCheese().getClass());
        assertEquals(reggianoCheese.toString(), nyPizzaFactory.createCheese().toString());

    }

    @Test
    public void testCreateSauce() {

        MarinaraSauce marinaraSauce = new MarinaraSauce();
        assertEquals(marinaraSauce.getClass(), nyPizzaFactory.createSauce().getClass());
        assertEquals(marinaraSauce.toString(), nyPizzaFactory.createSauce().toString());

    }

    @Test
    public void testCreateVeggie() {

        Veggies[] veggies = {new Garlic(), new Onion(), new Mushroom(), new RedPepper()};
        Veggies[] nyPizzaVeggies = nyPizzaFactory.createVeggies();
        IntStream.range(0, veggies.length).forEach(i -> assertEquals(veggies[i].toString(),
                                                   nyPizzaVeggies[i].toString()));

    }

    @Test
    public void testCreateClam() {

        FreshClams freshClams = new FreshClams();
        assertEquals(freshClams.getClass() , nyPizzaFactory.createClam().getClass());
        assertEquals(freshClams.toString() , nyPizzaFactory.createClam().toString());
    }
}
