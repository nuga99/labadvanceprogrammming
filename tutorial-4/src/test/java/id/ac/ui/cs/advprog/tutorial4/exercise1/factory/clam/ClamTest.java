package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import org.junit.Before;
import org.junit.Test;



public class ClamTest {

    private FreshClams freshClams;
    private FrozenClams frozenClams;
    private GiantClam giantClam;

    @Before
    public void setUp() {

        freshClams = new FreshClams();
        frozenClams = new FrozenClams();
        giantClam = new GiantClam();
    }

    @Test
    public void testClamIngredientName() {

        assertEquals(freshClams.toString(), "Fresh Clams from Long Island Sound");
        assertEquals(frozenClams.toString(), "Frozen Clams from Chesapeake Bay");
        assertEquals(giantClam.toString(), "Giant Clam");
    }


}
