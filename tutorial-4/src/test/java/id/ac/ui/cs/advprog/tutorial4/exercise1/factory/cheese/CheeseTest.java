package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.BigCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import org.junit.Before;
import org.junit.Test;

public class CheeseTest {

    private BigCheese bigCheese;
    private MozzarellaCheese mozzarellaCheese;
    private ParmesanCheese parmesanCheese;
    private ReggianoCheese reggianoCheese;

    @Before
    public void setUp() {

        bigCheese = new BigCheese();
        mozzarellaCheese = new MozzarellaCheese();
        parmesanCheese = new ParmesanCheese();
        reggianoCheese = new ReggianoCheese();

    }

    @Test
    public void testCheeseIngredientName() {

        assertEquals(bigCheese.toString(), "Big Cheese");
        assertEquals(mozzarellaCheese.toString(), "Shredded Mozzarella");
        assertEquals(parmesanCheese.toString(), "Shredded Parmesan");
        assertEquals(reggianoCheese.toString(), "Reggiano Cheese");
    }

}
