package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;




public class PizzaOrderTest {

    private NewYorkPizzaStore nyStore;
    private DepokPizzaStore depokStore;

    @Before
    public void setUp() throws Exception {

        nyStore = new NewYorkPizzaStore();
        depokStore = new DepokPizzaStore();
    }

    @Test
    public void testPizzaCheeseOrder() {

        Pizza pizza1 = depokStore.createPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", pizza1.getName());

        Pizza pizza2 = nyStore.createPizza("cheese");
        assertEquals("New York Style Cheese Pizza", pizza2.getName());

    }


    @Test
    public void testPizzaCalmCheeseOrder() {

        Pizza pizza1 = depokStore.createPizza("clam");
        assertEquals("Depok Style Clam Pizza", pizza1.getName());

        Pizza pizza2 = nyStore.createPizza("clam");
        assertEquals("New York Style Clam Pizza", pizza2.getName());

    }

    @Test
    public void testPizzaVeggieOrder() {

        Pizza pizza1 = depokStore.createPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", pizza1.getName());

        Pizza pizza2 = nyStore.createPizza("veggie");
        assertEquals("New York Style Veggie Pizza", pizza2.getName());

    }


}
