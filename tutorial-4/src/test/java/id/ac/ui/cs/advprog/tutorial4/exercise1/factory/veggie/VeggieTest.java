package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggie;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Lettuce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import org.junit.Before;
import org.junit.Test;


public class VeggieTest {

    private BlackOlives blackOlives;
    private Eggplant eggplant;
    private Garlic garlic;
    private Lettuce lettuce;
    private Mushroom mushroom;
    private Onion onion;
    private RedPepper redPepper;
    private Spinach spinach;

    @Before
    public void setUp() {

        blackOlives = new BlackOlives();
        eggplant = new Eggplant();
        garlic = new Garlic();
        lettuce = new Lettuce();
        mushroom = new Mushroom();
        onion = new Onion();
        redPepper = new RedPepper();
        spinach = new Spinach();
    }

    @Test
    public void testVeggieIngredientName() {

        assertEquals(blackOlives.toString(), "Black Olives");
        assertEquals(eggplant.toString(), "Eggplant");
        assertEquals(garlic.toString(), "Garlic");
        assertEquals(lettuce.toString(), "Lettuce");
        assertEquals(mushroom.toString(), "Mushrooms");
        assertEquals(onion.toString(), "Onion");
        assertEquals(redPepper.toString(), "Red Pepper");
        assertEquals(spinach.toString(), "Spinach");

    }

}
