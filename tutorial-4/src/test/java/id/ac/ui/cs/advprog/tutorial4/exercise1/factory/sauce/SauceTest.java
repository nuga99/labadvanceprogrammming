package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MustardSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import org.junit.Before;
import org.junit.Test;



public class SauceTest {

    private MustardSauce mustardSauce;
    private MarinaraSauce marinaraSauce;
    private PlumTomatoSauce plumTomatoSauce;


    @Before
    public void setUp() {

        mustardSauce = new MustardSauce();
        marinaraSauce = new MarinaraSauce();
        plumTomatoSauce = new PlumTomatoSauce();

    }

    @Test
    public void testSauceIngredientName() {

        assertEquals(mustardSauce.toString(), "Mustard Sauce");
        assertEquals(marinaraSauce.toString(), "Marinara Sauce");
        assertEquals(plumTomatoSauce.toString(), "Tomato sauce with plum tomatoes");


    }

}
