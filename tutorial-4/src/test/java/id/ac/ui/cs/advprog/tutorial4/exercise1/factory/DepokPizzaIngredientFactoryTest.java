package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.BigCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.MediumCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MustardSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Lettuce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;


public class DepokPizzaIngredientFactoryTest {

    private DepokPizzaIngredientFactory depokPizzaFactory;

    @Before
    public void setUp() {

        depokPizzaFactory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testCreateDough() {

        MediumCrustDough mediumCrustDough = new MediumCrustDough();
        assertEquals(mediumCrustDough.getClass(), depokPizzaFactory.createDough().getClass());
        assertEquals(mediumCrustDough.toString(), depokPizzaFactory.createDough().toString());

    }

    @Test
    public void testCreateCheese() {

        BigCheese bigCheese = new BigCheese();
        assertEquals(bigCheese.getClass(), depokPizzaFactory.createCheese().getClass());
        assertEquals(bigCheese.toString(), depokPizzaFactory.createCheese().toString());

    }

    @Test
    public void testCreateSauce() {

        MustardSauce mustardSauce = new MustardSauce();
        assertEquals(mustardSauce.getClass(), depokPizzaFactory.createSauce().getClass());
        assertEquals(mustardSauce.toString(), depokPizzaFactory.createSauce().toString());

    }

    @Test
    public void testCreateVeggie() {

        Veggies[] veggies = {new Lettuce(), new Eggplant(), new Mushroom(),
                             new Spinach(), new Onion()};

        Veggies[] depokPizzaVeggies = depokPizzaFactory.createVeggies();

        for (int i = 0; i < veggies.length; i++) {
            assertEquals(veggies[i].toString(), depokPizzaVeggies[i].toString());
        }

    }

    @Test
    public void testCreateClam() {

        FrozenClams frozenClams = new FrozenClams();
        assertEquals(frozenClams.getClass(), depokPizzaFactory.createClam().getClass());
        assertEquals(frozenClams.toString(), depokPizzaFactory.createClam().toString());
    }


}
