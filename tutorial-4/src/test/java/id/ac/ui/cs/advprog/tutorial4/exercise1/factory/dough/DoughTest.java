package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.MediumCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import org.junit.Before;
import org.junit.Test;



public class DoughTest {

    private MediumCrustDough mediumCrustDough;
    private ThickCrustDough thickCrustDough;
    private ThinCrustDough thinCrustDough;

    @Before
    public void setUp() {

        mediumCrustDough = new MediumCrustDough();
        thickCrustDough = new ThickCrustDough();
        thinCrustDough = new ThinCrustDough();
    }

    @Test
    public void testDoughIngredientName() {

        assertEquals(mediumCrustDough.toString(), "Medium Crust Dough");
        assertEquals(thickCrustDough.toString(), "ThickCrust style extra thick crust dough");
        assertEquals(thinCrustDough.toString(), "Thin Crust Dough");

    }
}
