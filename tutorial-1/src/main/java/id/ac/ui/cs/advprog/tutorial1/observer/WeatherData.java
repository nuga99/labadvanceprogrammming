package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;
import java.util.Observer;

public class WeatherData extends Observable {

    private float temperature;
    private float humidity;
    private float pressure;

    public void measurementsChanged() {
        setChanged();
        notifyObservers();
    }

    public void setMeasurements(float temperature, float humidity,
                                float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    /**
     * Setter and Getter for Encapsulation.
     * Including:
     * <p>
     * - Temperature
     * - Humidity
     * - Pressure
     * </p>
     *
     */

    public float getTemperature() {

        return temperature;
    }

    public void setTemperature(float temperature) {

        this.temperature = temperature;
    }

    public float getHumidity() {

        return humidity;
    }

    public void setHumidity(float humidity) {

        this.humidity = humidity;
    }

    public float getPressure() {

        return pressure;
    }

    public void setPressure(float pressure) {

        this.pressure = pressure;
    }
}
