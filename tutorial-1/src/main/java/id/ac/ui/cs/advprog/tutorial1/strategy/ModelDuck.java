package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends id.ac.ui.cs.advprog.tutorial1.strategy.Duck {
    // TODO Complete me!

    public ModelDuck() {

        setFlyBehavior((FlyBehavior) new FlyNoWay());
        setQuackBehavior((QuackBehavior) new MuteQuack());
    }

    public void display() {

        System.out.println("I'm a model duck");
    }


}
