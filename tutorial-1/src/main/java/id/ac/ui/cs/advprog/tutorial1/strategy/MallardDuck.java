package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends id.ac.ui.cs.advprog.tutorial1.strategy.Duck {
    // TODO Complete me!

    public MallardDuck() {

        setFlyBehavior((FlyBehavior) new FlyWithWings());
        setQuackBehavior(new Quack());
    }

    public void display() {

        System.out.println("I'm a real Mallard duck");
    }
}
